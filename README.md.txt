Extractive Text Summarization with Streamlit

Streamlit is an open-source python framework used to deploy machine learning models, analyze and visualize datasets in an interactive manner.

We need to install Genism and Sumy libraries for text summarization. 
Gensim is a library that is used for summarizing texts and is based on the ranks of text sentences using a variation of the TextRank algorithm. 
Similarly, Sumy is also a text Summarization library that contains few more NLP algorithms.